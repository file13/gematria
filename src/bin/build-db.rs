extern crate gematria;
extern crate rusqlite;

use gematria::*;
use rusqlite::Connection;

fn main() {
    let conn = Connection::open("gematria.db").unwrap();
    let esd = "esd".to_string();
    let gsd = "gsd".to_string();
    let hsd = "hsd".to_string();
    println!("Creating Sqlite3 Database.");
    create_db_table_from_text(&conn, &english_gematria, &esd, "english-all.txt");
    create_db_table_from_text(&conn, &greek_gematria, &gsd, "greek-all.txt");
    create_db_table_from_text(&conn, &hebrew_gematria, &hsd, "hebrew-all.txt");
}
