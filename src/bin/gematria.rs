extern crate clap;
extern crate gematria;
extern crate rusqlite;

use clap::{App, Arg};
use gematria::*;
use rusqlite::Connection;
use std::io;
use std::path::{Path, PathBuf};

/*
TODO:
remove plain unwrap's
*/

fn main() {
    // Clap command line options
    let matches = App::new("Gematria")
        .version("0.2")
        .author("Michael Rossi <dionysius.rossi@gmail.com>")
        .about("A Hermetic Gematria tool for English, Greek, and Hebrew.")
        .arg(
            Arg::with_name("database")
                .short("d")
                .long("database")
                .value_name("DATABASE")
                .help("Set the Sqlite3 database to read from (defaults to 'gematria.db').")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("interactive")
                .short("i")
                .long("interactive")
                .help("Interactive Mode: Query the DB in a REPL.")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose mode")
                .help("Verbose output.")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("WORDS")
                .help("The word or words to query.")
                .required_unless("interactive")
                .multiple(true)
                .index(1),
        )
        .get_matches();

    // Path to our Sqlite database
    let mut database;

    // Get the path of the current executable
    let exe_path = std::env::current_exe().unwrap();

    if matches.is_present("verbose") {
        println!("exe_path: '{:?}'", exe_path);
    }

    // Database defaults to same location as exe
    database = PathBuf::from(exe_path);
    database.pop(); // remove the exe itself from the path
    database.push("gematria.db"); // add the default db

    if matches.is_present("verbose") {
        println!("path: '{:?}'", database);
    }

    // Override database if given on command line
    if matches.is_present("database") {
        database = PathBuf::from(matches.value_of("database").unwrap().to_string());
    }

    // Reality check database
    if !Path::new(&database).is_file() {
        println!("[*** Error] Could not find database: '{:?}'", database);
        std::process::exit(1);
    }

    // Connect to the database
    let conn = match Connection::open(database) {
        Ok(v) => v,
        Err(why) => {
            println!("[*** Error] Could not open database: {}", why);
            std::process::exit(1);
        }
    };

    // Either begin an interactive loop or query from the command line
    if matches.is_present("interactive") {
        loop {
            println!("Lookup?");
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Bad input!");
            let word: String = input.trim().to_string();

            let gem = if word.is_empty() {
                println!("Farewell!");
                return; // quit!
            } else if word.chars().all(|i| i.is_numeric()) {
                (
                    word.trim().parse().expect("Error converting number"),
                    vec![],
                )
            } else if is_english_string(&word) {
                english_gematria_vec(&word)
            } else if is_greek_string(&word) {
                greek_gematria_vec(&word)
            } else if is_hebrew_string(&word) {
                hebrew_gematria_vec(&word)
            } else {
                (0, vec![])
            };
            print_query(&conn, &gem);
            println!("--- Oracle ---");
            print_random_query(&conn, gem.0);
            println!("");
        }
    } else {
        // Query from the command line, we need to split each word
        for i in matches.values_of("WORDS").unwrap() {
            let word = i.to_string();
            let gem = if word.chars().all(|i| i.is_numeric()) {
                (
                    word.trim().parse().expect("Error converting number"),
                    vec![],
                )
            } else if is_english_string(&word) {
                english_gematria_vec(&word)
            } else if is_greek_string(&word) {
                greek_gematria_vec(&word)
            } else if is_hebrew_string(&word) {
                hebrew_gematria_vec(&word)
            } else {
                (0, vec![])
            };
            print_query(&conn, &gem);
            println!("--- Oracle ---");
            print_random_query(&conn, gem.0);
            println!("");
        }
    }
}
