//#![windows_subsystem = "windows"]

extern crate gtk;

use gtk::prelude::*;

use gtk::*;

fn main() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }

    let window = Window::new(WindowType::Toplevel);
    window.set_title("Gematria");
    window.set_default_size(500, 300);

    let vbox = Box::new(Orientation::Vertical, 2);
    let input_label = Label::new("Input");
    vbox.add(&input_label);

    let input = TextView::new();
    vbox.add(&input);
    vbox.set_child_expand(&input, true);
    vbox.set_child_fill(&input, true);
    vbox.set_child_padding(&input, 200);
    //vbox.set_child_pack_type(&input, PackType::End);
    let button = Button::new_with_label("Click me!");
    vbox.add(&button);

    window.add(&vbox);
    window.show_all();

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    button.connect_clicked(|_| {
        println!("Clicked!");
    });

    gtk::main();
}
