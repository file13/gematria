# gematria

Gematria is a library and command line tool to perform Hermetic style gematria for number and words in English, Ancient Greek, and Biblical Hebrew.

## Installation

Windows executables are included in the data folder.

Otherwise install 2018 edition of Rust (tested on Rust 1.37.0, so that or later should work) and run:

```
cargo build --release
```

The gematria executable will be in target/release.

There is a pre-made gematria Sqlite3 dictionary in data (gematria.db).

The gematria executable expects it to be in the same directory as it.


## Usage

To see all options use:

`gematria -h`

Run it on the command line along with the word.  To explore the magical word 'Fred', do:

`gematria Fred`

Alternatively you can run it in interactive mode with `-i`.  You then simply enter a blank line to exit.

## Examples

Here's a few simple examples of the output generated.

#### A number query: 42

42 | 6 | (2 * 3 * 7) => Ahlab, BM, Balah, Bali, Elbe, LBJ, MB, Mb, babble, bail, baled, blade, deified, diked, edified, flea, gel, glad, hike, jigged, leaf, leg,
Βααλγαδ, Βαλδαδ, γαλῆ, δίκη, δίκη, δίκῃ, θααλα, κακά, κακὰ, κακά, ἅμα,
אֱלֹֽוהַּ, אֱלֹוהַ, אֱלֹוהַּ, אֶלאִדֹּו, אָהֳלֹֽו, אָהֳלֹו, בְגָזֵל, בְיַדיְהוָה, בִלְהָֽה, בִלְהָה, בַלָּהָה, בְּֽיַדיְהוָה, בְּֽלִי, בְּחֶבֶל, בְּיַדיְהוָה, בְּלִי, בִּכְיִֽי, בִּלְבָבֹֽו, בִּלְבָבֹו, בִּלְהָה, בֶּֽהָלָה, בֶּֽחָלָב, בַּחֲלֵב, בַּחֶבֶל, בַּלָּאט, בָּֽטְלָא, גְדֹלָֽה, גְדֹלָה, גָדְלָה, גְּדֹלָֽה, גְּדֹלָה, גִּדֵּֽלָה, גָּדְלָה, הֲלֹוא, הֵיטִיבוּ, הַגָּדֹל, הַהֶבֶל, הַלַּהַב, הַלָּֽז, הַלָּז, הֹואֵל, הֹואֶל, הֽוּאהִכָּה, הוּאהִכָּה, וְאֵלָה, וְאֵלֶּה, וְאֹהֶל, וְהִכָּהוּ, וְהָאֵל, וְהֹודַוְיָה, וְיַכּוּ, וְכִבְדֵי, וְלֵאָה, וְלֹו, וְלוּ, וַאֲחִיטוּב, וַיִּכְבַּד, וַיַּכְבֵּד, וַיַּכֹּו, וַיַּכּוּ, וַיֻּכּוּ, וָאֹהֶל, וּבְלֵבָב, וּלְבַד, זֶֽהיִהְיֶה, זֶהיִהְיֶה, חֲדַל, חֲדָֽל, חֲדָל, חֵלֶד, חָֽדֶל, חָֽלֶד, חָדַל, חָלֶד, טָֽבְאֵל, יְכַבְּדוּ, יִכְבְּדוּ, יֹוכֶבֶד, כְבֹודִי, כְּבֹודִי, כִּֽיבִי, כִּֽיהַגֵּד, כִּֽיהוּא, כִּֽיזֶה, כִּיבִי, כִּיהוּא, כִּיזֶה, לְאַחְאָֽב, לְאַחְאָב, לְבַדֹּֽו, לְבַדֹּו, לְבַטֵּא, לְגַגֹּו, לְגֹוג, לְדַאֲבָה, לְחָגָּא, לְחֹבָב, לִבִּֽי, לִבִּי, לֶֽהֱוֵא, לֶֽהָבָה, לֶהֱוֵֽא, לֶהֱוֵא, לֶהָבָֽה, לֶהָבָה, לָזֶה,

#### An English word query: Trump

700 = [200, 90, 300, 40, 70] | 7 | (2^2 * 5^2 * 7) => Colfax, Evert, Goodwill, Lusitania, Paracelsus, Schopenhauer, Seward, Sukkot, Trump, Tut, UV, Vaselines, Wimbledon, Winnipeg, Y, advert, approved, artists, assureds, calumniating, castrates, critiqued, deflower, descriptions, enumerated, ewers, firestorms, flowered, fluorite, fritters, goodwill, guillotine, housebroken, infiltrates, inoffensive, jettisoning, jowls, lovebirds, madwomen, militaristic, mouthpiece, mows, obstetrical, optimistic, palpitation, platinum, plentiful, pompous, precocious, preconceive, prepossess, remotest, removes, repulses, restest, resurfaces, revilings, rottener, serves, setters, severs, sewer, silvering, sleeplessness, slivering, sternness, straits, streets, stresses, terminates, tersest, testers, trump, tsunami, untapped, verses, waders, walloped, wearier, whimsical, worn, y,
Μαδιανίτιδος, Μανασσης, Ορνιου, βηρύλλιον, γενηθέντος, γραμματεῖς, γρηγορήσῃς, διεσκόρπισα, διηρπάζοντο, δάκνοντες, μανασσῆς, οἰκοδομήσῃς, οὔριον, παρρησίας, παρρησίας, περιελοῦ, ποθεινοτέρα, πυρὸν, φοῖνιξ, φόνοι, ἀκροατὴς, ἀναιρεθήσεται, ἀνήκοντας, ἀπάντησιν, ἀποκρύβηθι, ἀπάντησιν, ἀπάντησίν, ἅπτηται, ἐγκαταλειπόμενοι, ἐκδείραντες, ἐκκλινοῦμεν, ἐκλυόμενοι, ἐμπλακήσεται, ἐξιόντες, ἐπιτάγμασιν, ὑπέμενον, ῥύπον,
אֲשֶׁרהִפְקִיד, אֲשֶׁרצְדָקָה, בַּחֲצֵרֹֽת, בַּנְּחָלִים, הַמְאָדָּמִים, וְאַלתַּסְגֵּר, וְחֶרְפָּתֹו, וְחֶרְפֹּות, וְיֹדְעִים, וְרָֽדַפְתִּי, וְתַחְפְּרוּ, וִֽידֻעִים, וִידֻעִים, וּבִלְבֵנִים, וּבִנְבָלִים, וּמִמַּעֲמָֽדְךָ, יְרִיעֹתָֽי, יִתְפָּרְדוּ, יִתְפָּרָֽדוּ, יַדמְרֵמֹות, יֹֽודְעִים, יֹודְעִים, כַפֹּרֶת, כִּֽירָעַת, כִּיעַם, כַּפְתֹּר, כַּפֹּרֶת, לְכִנִּם, לְכֻלְּכֶם, לְמַכִּים, לְעַם, לְעָֽם, לְעָם, לְרָעֹת, לִתְרַע, לַאֲדֹֽנֵיהֶם, לַאֲדֹנֵיהֶם, לָעָֽם, לָעָם, לֹאנֹֽותְרָהבֹּו, מְלָכִֽים, מְלָכִים, מְקַלְלֶֽךָ, מִיַּמִּים, מִיָּמִים, מִיֹּמָיִם, מִכְמֹרֶת, מִכֵּלִים, מִכָּלהָֽאָדָם, מִמְכֶּרֶת, מִנְעָלֶיךָ, מִקְנֶיךָ, מַלְכִים, מָנִים, מֹנִֽים, מֹנִים, ן, נִכְלָם, סֻכִּיִּים, עֲלֵיפִֽיךָ, עִםיְהוּדָה, עַלצִדְקָתֹו, עַלשֵׁשׁ, עַלשָֽׁקֶר, עַלשָׁקֶר, עַםיְהוּדָה, עַצְמְךָ, עָכַרְתִּי, עָלַם, עָרְלַת, עָרַכְתִּי, עֻלָּם, פְעָמֶיךָ, פְעָמַיִךְ, פִקּוּדֶיךָ, פָרֹכֶת, פְּסָללְךָ, פִּקּוּדֶֽיךָ, פִּקּוּדֶיךָ, פָּרֹכֶת, פֹּכֶרֶת, צָֽרָתִי, צָרָתִי, קָֽרֶת, קָם, קָרֶת, קֻם, רְפִידָתֹו, רִנְנַת, רַךְ, רַקַּת, רָעָתֵכִי, רָצִיתָ, שְׁנֵֽיסָרִיסֵי, שִׁת, שֵֽׁת, שֵׁת, שָׁשָֽׁק, שָׁשָׁק, שָׁת, שֹׁת, שֹׂרֵר, תְּכַפֵּר, תְּכֻפָּֽר, תְּסַמֵּר, תִּרְמֹס, תֵּצְרִי, תַּכְרִיעַ,

#### A Greek word: Ιησούς

888 = [10, 8, 200, 70, 400, 200] | 6 | (2^3 * 3 * 37) => Constantinople, Garry, Jehezekel, Kwangju, Nationwide, adorably, arboretums, associative, bullshitted, depressives, devotions, equivalence, equivocal, evocations, factitious, ferociousness, happily, illuminations, inaugurals, kidneys, nationwide, pawnbroker, periwinkles, phony, precautions, predetermination, provident, reformulating, shortstop, slangy, spacesuits, supplicates, swindlers, telecommute, ulcerous, uncompromising, undisguised, unstoppable, unweighed, woodiness,
Βελσαττιμ, Λαβωεμαθ, Λεβωνα, αὐτάρκειαν, γείσους, δέδωκέν, δέδωκεν, διεφθάρησαν, διεφθάρησαν, διορύγμασιν, διέθρεψεν, διέθρεψέν, δέδωκεν, εἰρηνεύετε, καταράσσειν, κατελείφθη, κατελείφθη, κατέταξας, κτίστην, λέγων, ληνῷ, λυπηρὸς, λέγων, μεμολυσμένη, μερισθήσεται, παρακληθήσονται, παρακληθήσονται, πεπαιδευμένης, πλήρους, προσκαυθῇ, στερηθεῖσαν, συλλαβέσθαι, συνθήκας, σχοίη, φαρμακείαις, ἀγνοήσαντες, ἀνακαύσεις, ἀνταποθανεῖται, ἀποκτείναντα, ἐκβάλλω, ἐκβάλλω, ἐκπορεύσῃ, ἐλεγχόμενοι, ἐξουθενήκασιν, ἐπιβάλλουσιν, ἐπιβαίνουσιν, ἐπληθύνατε, ἐχάλασαν, ἔσχηκεν, ἔσχηκέν, ἥλων, ἰησοῦς, Ἰησοῦς, ὀφειλήσειν,
אֶתטַבַּעְתֹּו, אֶתנִבְלָתָהּ, אֶתנַבְלֻתָהּ, בֵּיתעֲבֹדַת, בֶּןאֶלְקָנָה, הַֽמְרַגְּלִים, הָֽאַרְבָּעִים, הָאַרְבָּעִֽים, וְיָֽרָבְעָם, וְיָרָבְעָם, וְכָלבִּרְכַּיִם, וְעִבְרִים, וַיַּֽעֲבִרֵם, וַתְּתַֽעֲבִי, וּבְכֶסֶף, וּבְעִירָֽם, וּבְעֵירֹם, וּבְתֹפֶת, וּבַכֶּסֶף, וּרְעֵבִים, וּתְכוּנָתֹו, חֹפֵף, יַעַבְרֽוּם, כְּנַףהָֽאֶחָד, לְרַחֲמִים, לִנְתִבֹות, לִרְמָחִים, לַֽחֲרָמִים, נֶֽגַעהַצָּרַעַת, פְּחֶתֶת, פִּתַּחְתָּ, שֻׁלְחָנֶךָ, תְּפַתֵּֽחַ, תְּפַתַּח, תִּפְתַּח, תִּפְתָּח, תִּפָּתַח,

#### A Hebrew word: יהוה

26 = [10, 5, 6, 5] | 8 | (2 * 13) => Hadid, Hagia, Hide, back, bagged, caddie, cicadae, deice, efface, fief, fife, haddah, hide, hied, jibe, jig,
Αγγιθ, Ζαβαδια, Ιαδαι, Ιαδια, αἰεὶ,
אֲחֻזֵי, אַכֶּה, בְיָדִי, בְּדָוִיד, בְּחֶטְאֹו, בְּיָדִֽי, בְּיָדִי, בִּידֵי, בַּכַּד, בֹּוזֵֽהוּ, גֹוזִי, דִֽידַהֲבָא, דִּֽיהֲוָא, דִּיהוּא, הִבִּֽיט, הִבִּיט, הֵיטֵֽב, הֵיטֵב, הַֽחַגִּי, הַבִּיט, הַבֵּיט, הֹויָה, וְֽהָיָה, וְהִבֹּוז, וְהַגְּבֹהָה, וְהַדָּוָה, וְהַהֹוד, וְהַטּוּ, וְהָיָה, וְיִדְאֶה, וְיָדֹו, וְיֹובָב, וֶהְיֵה, וַחֲזֵה, וַיִּגְבַּהּ, וַיָּבֹאגָד, וַיָּגָז, וָגֹזִּי, וּבְחֵטְא, וְֽהָיָה, חַגִּיָּה, חַחִי, חָדִיד, חֹוזֵה, יְֽהוָה, יְהֹוִֽה, יְהֹוִה, יְהֹוָֽה, יְהֹוָה, יְהוִֽה, יְהוִה, יְהוָֽה, יְהוָה, יְהוָֽה, יֱהֹוִה, יֱהוִה, יֹאחֵז, יֹודוּ, כָבֵד, כְּבֹד, כְּדֹב, כֶּבֶד, כַּבֵּד, כָּבֵד, כָּבֹד, כֹּבֶד,

### TODO

...

## License

Copyright © 2019 Michael Rossi
BSD license.
