//! #A library to compute the gematria of words in the Western Hermetic tradition.
//! Currently, this library supports gematria for English, Greek, and Hebrew.
//!
//! It also includes two other crates containing the following executables:
//!
//! 1. **build_db:** a tool to create a Sqlite3 database from texts files.
//! 2. **gematria:** a command line interface to explore gematria.
//!
//! Please note that *gematria_cmd* might not render correctly in your terminal
//! unless it can display unicode fonts.

extern crate rand;
extern crate rusqlite;
extern crate unicode_normalization;

use rand::Rng;
use rusqlite::types::ToSql;
use rusqlite::NO_PARAMS;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use unicode_normalization::UnicodeNormalization;

/*
TODO:

Fix English to handle accents, etc.
Better printing/formatting with extras.
Better oracle version based on random selection of words.
GUI?
Webpage?

*/

/*******************************************************************************
 * Gematria computation functions & English Gematia
 ******************************************************************************/

/// Computes the digital root of n.
pub fn digital_root(n: u32) -> u32 {
    if n == 0 {
        0
    } else {
        let r = n % 9;
        match r == 0 {
            true => 9,
            false => r,
        }
    }
}

// Compute the basic or single incremental gematria where each letter is
// one greater than the last.  This assumes the char is lowercase.
fn basic_gematria(c: char) -> u32 {
    let lower_magic_ascii = 96; // What to subtract so a=1, -> 'a'-1
    let upper_magic_ascii = 122; // Max |ASCII| number, 'z'
    let i = c as u32;

    match i < lower_magic_ascii || i > upper_magic_ascii {
        true => 0,
        false => i - lower_magic_ascii,
    }
}

// Compute the gematria of an English character according to the standard method
// where it goes by ones, tens, then hundreds.  Assumes lowercase char.
fn std_gematria(c: char) -> u32 {
    let n = basic_gematria(c);
    if n < 1 {
        return 0;
    } else if n < 11 {
        return n;
    } else if n < 19 {
        return digital_root(n) * 10;
    } else if n < 27 {
        return digital_root(n) * 100;
    } else {
        return 0;
    }
}

// Compute the gematria of a string.  Assumes lowercase.
fn std_of_string(s: &str) -> u32 {
    s.chars().map(&std_gematria).sum::<u32>()
}

// Determines if a char is in the basic ASCII range.
fn is_english_char(c: char) -> bool {
    c as u32 <= 127
}

/// Determines if a string is an ASCII English string.
pub fn is_english_string(s: &str) -> bool {
    s.chars().all(is_english_char)
}

/// Strip a unicode char of any extra markings and return the lowercase version.
fn normalize_char(c: char) -> char {
    let n = c
        .to_string()
        .nfd()
        .collect::<String>()
        .chars()
        .nth(0)
        .unwrap();
    n.to_lowercase().next().unwrap()
}

/// Normalize a unicode string: Remove markings and render lowercase.
pub fn normalize_string(s: &str) -> String {
    s.chars().map(normalize_char).collect::<String>()
}

/// Compute the numeric gematria value for an English string.
pub fn english_gematria(s: &str) -> u32 {
    let n = normalize_string(&s);
    std_of_string(&n)
}

/// Compute the numeric gematria value for an English string and return a vec of the
/// values of each char.
///
/// **Example:**
///
/// ```
/// assert_eq!(gematria::english_gematria_vec(&"Archie".to_string()),
///            (116, vec!(1, 90, 3, 8, 9, 5)));
/// ```
pub fn english_gematria_vec(s: &str) -> (u32, Vec<u32>) {
    let n = normalize_string(&s);
    let r: Vec<u32> = n.chars().map(|c| std_gematria(c)).collect();
    let t = r.iter().sum();
    (t, r)
}

/*******************************************************************************
 * Greek Gematria
 ******************************************************************************/

// Determines if a char is a Greek char.
fn is_greek_char(c: char) -> bool {
    let i = c as u32;
    i >= 880 && i <= 1023 || i >= 7936 && i <= 8191 // basic & extended greek
}

/// Determines if a string is an Greek string.
pub fn is_greek_string(s: &str) -> bool {
    s.chars().all(is_greek_char)
}
#[test]
fn test_is_greek_string() {
    assert_eq!(is_greek_string("ιησους"), true);
    assert_eq!(is_greek_string("Ιησούς"), true);
    assert_eq!(is_greek_string("Shlomo"), false);
}

// Compute the Greek gematria for a char. Assumes normalized and lowercase.
fn std_greek_gematria(c: char) -> u32 {
    match c as u32 {
        945 => 1,
        946 => 2,
        947 => 3,
        948 => 4,
        949 => 5,
        989 => 6,
        950 => 7,
        951 => 8,
        952 => 9,
        953 => 10,
        954 => 20,
        955 => 30,
        956 => 40,
        957 => 50,
        958 => 60,
        959 => 70,
        960 => 80,
        985 => 90,
        991 => 90,
        961 => 100,
        962 => 200,
        963 => 200,
        964 => 300,
        965 => 400,
        966 => 500,
        967 => 600,
        968 => 700,
        969 => 800,
        993 => 900,
        _ => 0,
    }
}
#[test]
fn test_std_greek_gematria() {
    assert_eq!(std_greek_gematria('α'), 1);
}

/// Computes the gematria value of a Greek string.
pub fn greek_gematria(s: &str) -> u32 {
    match is_greek_string(&s) {
        true => {
            let n = normalize_string(&s);
            n.chars().map(&std_greek_gematria).sum::<u32>()
        }
        false => 0,
    }
}
#[test]
fn test_greek_gematria() {
    assert_eq!(greek_gematria("ιησους"), 888);
    assert_eq!(greek_gematria("Ιησούς"), 888);
    assert_eq!(greek_gematria("Ιησούς<loves!()"), 0);
}

/// Compute the numeric gematria value for an English string and return a vec of the
/// values of each char.
pub fn greek_gematria_vec(s: &str) -> (u32, Vec<u32>) {
    let n = normalize_string(&s);
    let r: Vec<u32> = n.chars().map(|c| std_greek_gematria(c)).collect();
    let t = r.iter().sum();
    (t, r)
}

/*******************************************************************************
 * Hebrew Gematria
 ******************************************************************************/

// Determines if a char is a Hebrew char.
fn is_hebrew_char(c: char) -> bool {
    let i = c as u32;
    i >= 1424 && i <= 1535 || i >= 64256 && i <= 64335 // basic & extended Hebrew
}

/// Determines if a string is an Hebrew string.
pub fn is_hebrew_string(s: &str) -> bool {
    s.chars().all(is_hebrew_char)
}
#[test]
fn test_is_hebrew_string() {
    assert_eq!(is_hebrew_string("בְּרֵאשִׁ֖ית"), true);
    assert_eq!(is_hebrew_string("הָאָֽרֶץ"), true);
    assert_eq!(is_hebrew_string("Shlomo"), false);
}

// Compute the Hebrew gematria for a char. Assumes normalized.
fn std_hebrew_gematria(c: char) -> u32 {
    match c as u32 {
        1488 => 1,
        1489 => 2,
        1490 => 3,
        1491 => 4,
        1492 => 5,
        1493 => 6,
        1494 => 7,
        1495 => 8,
        1496 => 9,
        1497 => 10,
        1499 => 20,
        1500 => 30,
        1502 => 40,
        1504 => 50,
        1505 => 60,
        1506 => 70,
        1508 => 80,
        1510 => 90,
        1511 => 100,
        1512 => 200,
        1513 => 300,
        1514 => 400,
        1498 => 500,
        1501 => 600,
        1503 => 700,
        1507 => 800,
        1509 => 900,
        _ => 0,
    }
}
#[test]
fn test_std_hebrew_gematria() {
    assert_eq!(std_hebrew_gematria('א'), 1);
}

/// Computes the gematria value of a Hebrew string.
pub fn hebrew_gematria(s: &str) -> u32 {
    match is_hebrew_string(&s) {
        true => {
            let n = normalize_string(&s);
            n.chars().map(&std_hebrew_gematria).sum::<u32>()
        }
        false => 0,
    }
}
#[test]
fn test_hebrew_gematria() {
    assert_eq!(hebrew_gematria("בָּרָ֣א"), 203);
    assert_eq!(
        hebrew_gematria("</sup> אֲשֶׁ֥ר זַרְעֹו־בֹ֖ו לְמִינֵ֑"),
        0
    );
    assert_eq!(hebrew_gematria("colorblue"), 0);
}

/// Compute the numeric gematria value for an English string and return a vec of the
/// values of each char.
pub fn hebrew_gematria_vec(s: &str) -> (u32, Vec<u32>) {
    let n = normalize_string(&s);
    let r: Vec<u32> = n.chars().map(|c| std_hebrew_gematria(c)).collect();
    let t = r.iter().sum();
    (t, r)
}

/*******************************************************************************
 * Importing data & Sqlite DB creation
 ******************************************************************************/

pub type GematriaMap = HashMap<u32, HashSet<String>>;

// Imports a text file into an in GematriaMap. This can then be directly queried,
// or more practically, dumped into a SQL database.
fn import_text_file<GF>(dict: &mut GematriaMap, gematria_fn: &GF, filename: &str)
where
    GF: Fn(&str) -> u32,
{
    let file = File::open(filename).unwrap();
    for line in BufReader::new(file).lines() {
        let words = line.unwrap();
        for s_word in words.split_whitespace() {
            let word = s_word.to_string().replace(|c: char| !c.is_alphabetic(), "");
            let key = gematria_fn(&word);
            if key != 0 {
                if dict.contains_key(&key) {
                    let d = dict.get_mut(&key).unwrap();
                    d.insert(word);
                } else {
                    let mut tmp = HashSet::new();
                    tmp.insert(word);
                    dict.insert(key, tmp);
                }
            }
        }
    }
}

// Create a Sqlite table.
fn create_table(conn: &rusqlite::Connection, table_name: &str) {
    println!("Creating table...");
    let sql = format!(
        "CREATE TABLE {} (
                       Number INTEGER,
                       Word TEXT)",
        table_name
    );
    conn.execute(&sql, NO_PARAMS).unwrap();
}

// Insert a GematriaMap into a Sqlite table.
fn insert_dict_into_db(conn: &rusqlite::Connection, table_name: &str, dict: &GematriaMap) {
    println!("Inserting data...");
    conn.execute("BEGIN TRANSACTION", NO_PARAMS).unwrap();

    let sql = format!(
        "INSERT INTO {} (number, word)
                       VALUES (?1, ?2)",
        table_name
    );
    for (k, v) in dict {
        for i in v {
            conn.execute(&sql, &[k, i as &dyn ToSql]).unwrap();
        }
    }

    conn.execute("COMMIT", NO_PARAMS).unwrap();
}

// Prints out a GematriaMap
fn print_dict(dict: &HashMap<u32, HashSet<String>>) {
    for (k, v) in dict {
        print!("{} => ", k);
        for i in v {
            print!("{}, ", i);
        }
        println!("");
    }
}

/// Creates a Sqlite3 table with a gematria function and table name from a text file.
///
/// This assumes the table does not already exist.
pub fn create_db_table_from_text<GF>(
    conn: &rusqlite::Connection,
    gematria_fn: &GF,
    table_name: &str,
    filename: &str,
) where
    GF: Fn(&str) -> u32,
{
    let mut dict: HashMap<u32, HashSet<String>> = HashMap::new();
    println!("Loading dictionary file: {} => {}", filename, table_name);
    import_text_file(&mut dict, gematria_fn, filename);
    print_dict(&dict);
    create_table(&conn, table_name);
    insert_dict_into_db(&conn, table_name, &dict);
}

/*******************************************************************************
 * Additional Numeric Conversions
 ******************************************************************************/

fn trial_factor_division(n: u32) -> Vec<u32> {
    let mut num = n;
    let mut to_check = 2; // begin with 1st prime
    let mut f: Vec<u32> = Vec::new();
    while to_check * to_check <= num {
        if num % to_check == 0 {
            //  see if divisible and thus a factor
            f.push(to_check);
            num = num / to_check; //  see if it's the last factor
        } else {
            to_check = to_check + 1;
        }
    }
    if num != 1 {
        f.push(num);
    } //  record last factor if any
    f.sort();
    f
}

#[test]
fn test_trial_factor_division() {
    assert_eq!(trial_factor_division(1), vec!());
    assert_eq!(trial_factor_division(6), vec!(2, 3));
    assert_eq!(trial_factor_division(7), vec!(7));
    assert_eq!(trial_factor_division(12), vec!(2, 2, 3));
    assert_eq!(trial_factor_division(13), vec!(13));
    assert_eq!(trial_factor_division(120), vec!(2, 2, 2, 3, 5));
    assert_eq!(trial_factor_division(121), vec!(11, 11));
    assert_eq!(trial_factor_division(158), vec!(2, 79));
}

fn simplify_factors(f: &Vec<u32>) -> String {
    let mut repeats = 0;
    let mut result = "".to_string();
    let mut current;
    for i in 0..f.len() {
        current = f[i];
        if i == 0 {
            result.push_str(&current.to_string());
        } else if i == f.len() - 1 {
            // last item
            if current == f[i - 1] {
                repeats += 1;
                result.push_str("^");
                result.push_str(&(repeats + 1).to_string());
            } else if repeats == 0 {
                result.push_str(" * ");
                result.push_str(&current.to_string());
            } else {
                result.push_str("^");
                result.push_str(&(repeats + 1).to_string());
                result.push_str(" * ");
                result.push_str(&current.to_string());
            }
        } else {
            //  if another repeat
            if current == f[i - 1] {
                repeats += 1;
            } else {
                //  if not another repeat
                if repeats == 0 {
                    result.push_str(" * ");
                    result.push_str(&current.to_string());
                } else {
                    result.push_str("^");
                    result.push_str(&(repeats + 1).to_string());
                    result.push_str(" * ");
                    result.push_str(&current.to_string());
                    repeats = 0;
                }
            }
        }
    }
    result
}

#[test]
fn test_simplify_factors() {
    assert_eq!(simplify_factors(&vec!(2, 3)), "2 * 3");
    assert_eq!(simplify_factors(&vec!(7)), "7");
    assert_eq!(simplify_factors(&vec!(2, 2, 3)), "2^2 * 3");
    assert_eq!(simplify_factors(&vec!(13)), "13");
    assert_eq!(simplify_factors(&vec!(2, 2, 2, 3, 5)), "2^3 * 3 * 5");
    assert_eq!(simplify_factors(&vec!(11, 11)), "11^2");
    assert_eq!(simplify_factors(&vec!(2, 79)), "2 * 79");
}

fn format_factors(f: &Vec<u32>, simplify_result: bool) -> String {
    let len = f.len();
    let mut result = "".to_string();
    let mut no_simplify = false;

    if len == 0 {
        no_simplify = true;
        result = "?".to_string();
    } else if len == 1 {
        no_simplify = true;
        result = "Prime".to_string();
    }

    if !no_simplify && simplify_result {
        result = simplify_factors(f);
    } else if len > 1 {
        // --  add *'s between numbers
        for i in 0..len {
            //  if not last factor
            if i != (len - 1) {
                result.push_str(&f[i].to_string());
                result.push_str(" * ");
            } else {
                //  if last factor
                result.push_str(&f[i].to_string());
            }
        }
    }
    result
}

#[test]
fn test_format_factors() {
    assert_eq!(format_factors(&vec!(), true), "?");
    assert_eq!(format_factors(&vec!(2, 3), true), "2 * 3");
    assert_eq!(format_factors(&vec!(7), true), "Prime");
    assert_eq!(format_factors(&vec!(2, 2, 3), true), "2^2 * 3");
    assert_eq!(format_factors(&vec!(2, 2, 3), false), "2 * 2 * 3");
    assert_eq!(format_factors(&vec!(13), true), "Prime");
    assert_eq!(format_factors(&vec!(2, 2, 2, 3, 5), true), "2^3 * 3 * 5");
    assert_eq!(
        format_factors(&vec!(2, 2, 2, 3, 5), false),
        "2 * 2 * 2 * 3 * 5"
    );
    assert_eq!(format_factors(&vec!(2, 79), true), "2 * 79");
    assert_eq!(format_factors(&vec!(2, 3, 3, 3, 7), true), "2 * 3^3 * 7");
}

fn factor(n: u32, simplify_result: bool) -> String {
    let f = trial_factor_division(n);
    format_factors(&f, simplify_result)
}

#[test]
fn test_factor() {
    assert_eq!(factor(1, true), "?");
    assert_eq!(factor(6, true), "2 * 3");
    assert_eq!(factor(7, true), "Prime");
    assert_eq!(factor(12, true), "2^2 * 3");
    assert_eq!(factor(12, false), "2 * 2 * 3");
    assert_eq!(factor(13, false), "Prime");
    assert_eq!(factor(120, true), "2^3 * 3 * 5");
    assert_eq!(factor(120, false), "2 * 2 * 2 * 3 * 5");
    assert_eq!(factor(121, true), "11^2");
    assert_eq!(factor(144, true), "2^4 * 3^2");
    assert_eq!(factor(158, true), "2 * 79");
    assert_eq!(factor(378, true), "2 * 3^3 * 7");
    assert_eq!(factor(1587, true), "3 * 23^2");
    assert_eq!(factor(1995, true), "3 * 5 * 7 * 19");
    assert_eq!(factor(2000, true), "2^4 * 5^3");
    assert_eq!(factor(2058, true), "2 * 3 * 7^3");
    assert_eq!(factor(2352, true), "2^4 * 3 * 7^2");
    assert_eq!(factor(2448, true), "2^4 * 3^2 * 17");
    assert_eq!(factor(2503, true), "Prime");
}

/*******************************************************************************
 * DB query functions
 ******************************************************************************/

// for our DB queries
#[derive(Debug)]
pub struct Gematria {
    number: i32,
    word: String,
}

/// Returns all the words matching the number from a Sqlite table.
pub fn query(conn: &rusqlite::Connection, table_name: &str, number: u32) -> Vec<String> {
    let sql = format!(
        "SELECT number, word FROM {} where number = {} order by word",
        table_name, number
    );
    let mut stmt = conn.prepare(&sql[..]).unwrap();
    let gematria_iter = stmt
        .query_map(NO_PARAMS, |row| {
            Ok(Gematria {
                number: row.get(0).unwrap(),
                word: row.get(1).unwrap(),
            })
        })
        .unwrap();
    let r = gematria_iter.map(|i| i.unwrap().word).collect();
    r
}

/// Prints all the words matching the number from a Sqlite table to stdio.
pub fn print_query(conn: &rusqlite::Connection, gematria: &(u32, Vec<u32>)) {
    let tables = ["esd", "gsd", "hsd"];
    let number = gematria.0;
    let nums = &gematria.1;
    match nums.is_empty() {
        true => print!(
            "{} | {} | ({}) => ",
            number,
            digital_root(number),
            factor(number, true)
        ),
        false => print!(
            "{} = {:?} | {} | ({}) => ",
            number,
            nums,
            digital_root(number),
            factor(number, true)
        ),
    }
    for table in tables.iter() {
        let r = query(&conn, &table, number);
        for i in r {
            print!("{}, ", i);
        }
        println!("");
    }
}

/// Prints a randomly selected word from each language from a Sqlite table to stdio.
pub fn print_random_query(conn: &rusqlite::Connection, number: u32) {
    let tables = ["esd", "gsd", "hsd"];
    println!("{} => ", number);
    for table in tables.iter() {
        let r = query(&conn, &table, number);
        let i = rand::thread_rng().gen_range(0, r.len());
        print!("{}, ", r[i]);
    }
    println!("");
}
